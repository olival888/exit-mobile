/**
 * Created by Олег on 23.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	if(!location.hash){
		location.hash="casino";
	}else{
		let curElem=document.querySelector(location.hash);

		curElem.style.display="block";
		window.onhashchange=()=>{
			curElem.removeAttribute("style");
		}
	}
});
/**
 * Created by ���� on 19.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	if(window.matchMedia("(orientation: portrait)").matches){
		(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/timetable/portrait")).loadBackground()
			.then(()=>(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/timetable/landscape")).loadBackground());
	}else{
		(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/timetable/landscape")).loadBackground()
			.then(()=>(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/timetable/portrait")).loadBackground());
	}

	new Scroller(document.querySelector('.date-list'));
});
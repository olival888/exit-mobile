"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	if(window.matchMedia("(orientation: portrait)").matches){
		(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/booking/portrait")).loadBackground()
			.then(()=>(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/booking/landscape")).loadBackground());
	}else{
		(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/booking/landscape")).loadBackground()
			.then(()=>(new BackgroundLoad(document.querySelector(".quest__img_role_bg"), "/images/media/booking/portrait")).loadBackground());
	}
});
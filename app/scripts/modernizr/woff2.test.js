"use strict";

Modernizr.addTest('woff2', () => {
	let f = new FontFace('t', 'url("data:application/font-woff2,") format("woff2")', {});

	try {
		f.load();
	}catch(e) {
		throw e;
	}

	return f.status == 'loading';
});

"use strict";

Modernizr.testStyles('#modernizr { height: 10px;}#content { height: calc(100% - 1px);}', (elem, rule)=>{
  Modernizr.addTest('calcPercent', elem.children.content.offsetHeight > 0);
},1,["content"]);

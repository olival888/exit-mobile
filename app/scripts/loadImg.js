"use strict";

class ImgLoad{
	constructor(elem, folder, storage="none") {
		function makeUrl(fileName, folder) {
			let getFormat = ()=>{
				if(document.documentElement.classList.contains("webp")){
					return "webp";
				/*}else if(Modernizr.jp2){
					return "jp2";*/
				}else{
					return "jpg";
				}
			};

			let resolutionFolder = (screenWidth)=>{
				if(screenWidth>=900){
					return "1000";
				}else if(screenWidth>=650){
					return "800";
				}else if(screenWidth>=500){
					return "650";
				}else{
					return "500";
				}
			};

			return `${folder}/${getFormat()}/2x/${resolutionFolder(window.screen.width)}/${fileName}.${getFormat()}`;
		}

		this.src=makeUrl(elem.dataset.src, folder);
		this.storage=storage;
		this.elem=elem;
	}

	checkBlobSupport(){
		return (Modernizr.blobconstructor && Modernizr.bloburls)?
					Promise.resolve():
					Promise.reject();
	}

	fillSrc(){
		return new Promise((resolve, reject) => {
			this.elem.onload=()=>{
				resolve(this)
			};
			this.elem.onerror=()=>{
				reject(this)
			};

			this.elem.src=this.src;
		});
	}

	fillSrcBlob(response) {
		return new Promise((resolve, reject) => {
			this.elem.onload=()=>{
				resolve(this)
			};
			this.elem.onerror=()=>{
				reject(this)
			};

			this.elem.src=window.URL.createObjectURL(new Blob([response]));
		});
	}



	checkExistInLocalforage(){
		if (typeof localforage !== "undefined") {
			return localforage.getItem(this.src)
				.then((val)=>val);
		}else{
			return false;
		}
	}

	setInLocalforage(response){
		return localforage.setItem(this.src, response)
			.then(()=>Promise.resolve(response))
			.catch(()=>Promise.reject(response));
	}

	getBlobImg(){
		let request = new XMLHttpRequest();

		request.open("GET", this.src, true);
		request.responseType = 'arraybuffer';

		request.onload = ()=> {
			if (request.status >= 200 && request.status < 400) {
				return Promise.resolve(request.response);
			} else {
				return Promise.reject(Error(request.statusText));
			}
		};

		request.onerror = ()=> {
			return Promise.reject(Error("Сетевая ошибка"));
		};

		request.send();
	}

	successHendler(){
		this.elem.classList.add("complete");
		this.elem.classList.remove("pending");
		return Promise.resolve();
	}

	errorHendler(){
		this.elem.classList.add("error");
		return Promise.reject();
	}

	preloadImage() {
		this.elem.classList.add("pending");
		switch (this.storage){
			case "none":
				return this.fillSrc();

				break;
			case "local":
				return this.checkBlobSupport()
							.then(()=>this.checkExistInLocalforage())
							.then((val)=>!val ?
								this.getBlobImg().then((response)=>this.setInLocalforage(response)) :
								this.setInLocalforage(val))

							.then((response)=>this.fillSrcBlob(response))
							.catch((response)=>this.fillSrc(response));
				break;
		}
	}

	loadImage(){
		return this.preloadImage()
					.then(()=>this.successHendler())
					.catch(()=>this.errorHendler());
	}
}


function loadImages(elemArr, folder,storage) {
	if(elemArr.length){
		return Array.from(elemArr).map((elem)=>(new ImgLoad(elem, folder, storage)).preloadImage())
						.reduce((sequence, imagePromise) =>
							sequence.then(() =>imagePromise)
								.then((elem)=>elem.successHendler())
								.catch((elem)=>elem.errorHendler()),
							Promise.resolve());
	}else{
		return Promise.reject(Error("Массив картинок на загрузку пуст"));
	}
}

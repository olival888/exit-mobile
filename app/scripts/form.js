"use strict";



/*
* создать класс sendForm
* */
/*
 * написать события для валидации поля формы и формы
 * протестить функции getInfo и sendInfo
 *
 * плохая обработка ошибок
 * */

/*сделать методом узла*/


function chengeInput(input){
	if(input.type=="checkbox"){
		input.checked=!input.checked;
	}else if(input.type=="radio"){
		input.checked=true;
	}

	var event = document.createEvent('HTMLEvents');
	event.initEvent('change', true, false);
	input.dispatchEvent(event);
}

function getCookie(name) {
	let cookieValue = null;
	if (document.cookie) {
		var cookies = document.cookie.split(';');

		for(let cookie of cookies){
			cookie=cookie.trim();
			if (cookie.substring(0, name.length + 1) == (name + '=')) {
				cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
				break;
			}
		}
	}
	return cookieValue;
}

class Form{
	constructor(form,errMess=null){
		/*function csrfSafeMethod(method) {
			return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		}*/


		Array.from(form.querySelectorAll("input")).forEach((input)=>{
			input.addEventListener("blur", (e) => {
				let target = e.target;

				let fieldContaner = target.parentElement.parentElement;

				if (target.validity.valid) {
					fieldContaner.classList.remove("invalid");
					if(errMess){
						errMess.classList.remove("visible");
					}
				} else {
					fieldContaner.classList.add("invalid");
				}
			});
		});

		this.csrftoken=getCookie('csrftoken');

		this.form=form;
		this.method=form.dataset.method || "POST";
		this.action=form.action;

		this.response=null;//??????????

		this.errMess = errMess;
	}

	getCookie(name){
		return getCookie(name);
	}

	validateForm(){
		if (this.form.checkValidity()) {
			return Promise.resolve();
		} else {
			return Promise.reject();
		}
	}

	sendInfo(){
		return new Promise((resolve, reject) => {
			let info=new FormData(this.form);

			let submitButtons=this.form.querySelectorAll("button");

			Array.from(submitButtons).map((button)=>button.disabled=true);

			var request = new XMLHttpRequest();

			request.open(this.method, this.action, true);

			request.setRequestHeader('X-CSRFToken', this.csrftoken);

			request.onload = () => {
				Array.from(submitButtons).map((button)=>button.disabled=false);

				if (request.status >= 200 && request.status < 400) {
					this.response=JSON.parse(request.responseText);
					if (this.response.status == 'ok') {
						resolve();
					}else{
						reject();
					}
				} else {
					switch (request.status){
						case 401:
							if(document.querySelector("#auth-popup")){
								location.href="/login";
							}else{
								location.href="/";
							}
							break;
						case 403:
							this.csrftoken=this.getCookie('csrftoken');

							var event = document.createEvent('HTMLEvents');
							event.initEvent('submit', true, false);
							this.form.dispatchEvent(event);//протестить, потому что соысем не уверен в работе

							break;
						default :
							reject(Error(request.status));
					}

				}
			};

			request.onerror = ()=> {
				Array.from(submitButtons).map((button)=>button.disabled=false);

				reject(Error("Сетевая ошибка"));
			};

			request.send(info);
		});

	}

	markFormInvalid(mess=""){
		if(this.errMess){
			this.errMess.classList.add("visible");
		}

		return Promise.reject();
	}

	markFormValid(){
		if(this.errMess){
			this.errMess.classList.remove("visible");
		}

		return Promise.resolve();
	}

	onsubmit(cb){
		this.form.addEventListener("submit",(e)=>{
			cb();

			e.preventDefault();
		});
	}

	simpleSubmitForm(){
		return this.validateForm()
					.then(()=>this.markFormValid(),()=>this.markFormInvalid())
					.then(()=>this.sendInfo());
	}
}

function getInfo(url,type="GET"){
	return new Promise(function(resolve, reject) {
		var request = new XMLHttpRequest();
		request.open(type, url, true);

		request.onload = ()=> {
			if (request.status >= 200 && request.status < 400) {
				resolve(JSON.parse(request.responseText));
			} else {
				reject(Error(request.statusText));
			}
		};

		request.onerror = ()=> {
			reject(Error("Сетевая ошибка!"));
		};

		request.send();
	});
}

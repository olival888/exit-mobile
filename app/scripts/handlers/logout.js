/**
 * Created by Дом on 14.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=>{
	document.querySelector("#logout").addEventListener("click",(e)=>{
		getInfo('/api/logout/').then(function(response) {
			window.location = response.redirect_to;
		})
			.catch(function(error) {
				console.error("Не удалось выполнить!", error);
			});
	});
});

/**
 * Created by Дом on 14.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	document.querySelector("#abort-registration").addEventListener("click",()=>{
		getInfo('/api/abort-registration/',"DELETE")
			.then(function(response) {
				window.location = response.redirect_to;
			});
	});
});


/**
 * Created by Дом on 14.08.2015.
 */
"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	Array.from(document.querySelectorAll(".btn_role_delete-reserve")).forEach((btn)=>{
		btn.addEventListener("click",(e)=>{
			let btn=e.target;

			getInfo(`/api/reserve/${btn.dataset.reservePk}/delete`,"DELETE")
				.then(()=>{
					let li = btn.parentNode.parentNode.parentNode;
					let ul = li.parentNode;
					let container = ul.parentNode;
					if (ul.childNodes.length > 1){
						li.parentNode.removeChild(li);
					}else{
						container.parentNode.removeChild(container);
					}
				});
		});
	});
});

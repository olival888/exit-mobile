/**
 * Created by Дом on 13.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	let form = document.querySelector(".form_name_user-data");
	let sendBtn=form.querySelector("[type=submit]");

	function enableChangeBtn(){
		sendBtn.disabled=false;
	}

	function disableChangeBtn(){
		sendBtn.disabled=true;
	}

	Array.from(form.querySelectorAll("input")).forEach((input)=>{
		input.addEventListener("focus",(e)=> {
			Array.from(form.querySelectorAll("input")).forEach((input)=>{
				input.addEventListener("input",enableChangeBtn);
			});
		});
	});

	let formObj = new Form(form, form.previousElementSibling);

	formObj.onsubmit(()=>{
		formObj.simpleSubmitForm()
			.then(()=> {
				disableChangeBtn();
			},()=>{
				formObj.markFormInvalid();
			});
	});
});


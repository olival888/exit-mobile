/**
 * Created by Дом on 12.08.2015.
 */

"use strict";

document.addEventListener('DOMContentLoaded', ()=> {
	let form=document.querySelector(".form_name_reservation");
	let formObj = new Form(form);

	formObj.onsubmit(()=>{
		formObj.simpleSubmitForm()
			.then(()=> {
				window.location = formObj.response.redirect_to;
			});
	});

	document.querySelector(".btn_role_cancel").addEventListener("click",()=>{
		window.history.back();
	});
});

/**
 * Created by Олег on 22.12.2014.
 */

"use strict";

function Scroller(contaner) {
	//var contaner=document.querySelector('.date-list');

	var marginElem=contaner.children[0],
		initialMarginValue=parseFloat(getComputedStyle(marginElem, '').marginLeft);

	var itemWidth=contaner.children[0].clientWidth,
		contanerWidth=contaner.children.length*itemWidth;//+initialMarginValue*2;



	var validScrollValuesArr=[0];
	for(var i=0;i<(contaner.children.length-1);i++){
		validScrollValuesArr.push(validScrollValuesArr[i]+itemWidth);
	}

	Math.easeOutCirc = function(t, b, c, d) {
		return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
	};

	var requestAnimFrame = (function(){
		return  window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function( callback ){ window.setTimeout(callback, 1000 / 60); };
	})();

	function scrollTo(elem, to, speed, callback) {
		var start = elem.scrollLeft,
			change = to - start,
			currentTime = 0,
			increment = 20,
			duration = Math.abs(change)/speed*1000;

		if(duration>increment){
			document.body.classList.add("disable-pointer-events");
			var animateScroll = function() {
				// increment the time
				currentTime += increment;
				// find the value with the quadratic in-out easing function
				var val = Math.easeOutCirc(currentTime, start, change, duration);
				// move the element
				elem.scrollLeft = val;
				// do the animation unless its over
				if (currentTime < duration) {
					requestAnimFrame(animateScroll);
				} else {
					if (callback && typeof(callback) === 'function') {
						// the animation is done so lets callback
						callback();
					}
					document.body.classList.remove("disable-pointer-events");
				}
			};
			animateScroll();
		}
	}

	interact(contaner)
		.origin('self')
		.restrict({drag: 'self'})
		//.inertia(true)
		.draggable({
			axis: 'x',
			onstart: function (event) {

			},
			onmove : function (event) {
				var target = event.target;

				target.scrollLeft-=event.dx;
			},
			onend  : function (event) {
				var target = event.target;

				var curItemIndex=0;

				function checkList(curItemIndex){
					document.getElementById(event.target.children[curItemIndex].children[0].getAttribute('for')).checked=true;
				}

				/*var speed=45;px/1000ms*/
				if(event.swipe){

					var itemsCount=0;

					if(event.swipe.speed>1000 && event.swipe.speed<2000){
						itemsCount=1;
					}else if(event.swipe.speed>2000 && event.swipe.speed<3000){
						itemsCount=2;
					}else if(event.swipe.speed>3000){
						itemsCount=3;
					}

					var to=0;
					if(event.swipe.right){
						curItemIndex=Math.floor(target.scrollLeft/itemWidth)-itemsCount;
						to=curItemIndex*itemWidth;
						if(to<0){
							to=0;
						}
					}else{
						curItemIndex=Math.ceil(target.scrollLeft/itemWidth)+itemsCount;
						to=curItemIndex*itemWidth;
						if(to>(contanerWidth-itemWidth/2)){
							to=contanerWidth-itemWidth/2;
						}
					}

					scrollTo(target, to, 140,checkList.call(target,curItemIndex));
				}else{
					if(event.dx<0){
						curItemIndex=Math.ceil(target.scrollLeft/itemWidth);
					}else{
						curItemIndex=Math.floor(target.scrollLeft/itemWidth);
					}

					scrollTo(target, curItemIndex*itemWidth, 50,function(){checkList.call(target,curItemIndex)});
				}
			}
		})
		.on('tap', function (event) {
			var target = event.currentTarget;

			function checkList(){
				////event.target.parentNode.click();
				document.getElementById(event.target.parentNode.getAttribute('for')).checked=true;
			}

			var destPoint=event.clientX+target.scrollLeft-initialMarginValue;

			if((destPoint>0) && (event.target.tagName!='LI') && (target.className!=event.target.className)){
				scrollTo(target, Math.floor(destPoint/itemWidth)*itemWidth, 140, checkList);
			}
		});
};

document.addEventListener("click", function (event) {
	var target = event.target;

	while(target != this) {
		if (target.tagName == 'LABEL' && target.classList.contains('date')) {
			event.preventDefault();
			break;
		}
		target = target.parentNode;
	}
});


/**
 * Created by Дом on 04.08.2015.
 * Переделать через генераторы, добавить обработку ошибок на каждый скрипт
 */

"use strict";

class ScriptLoad{
	constructor(src,storage="none") {
		this.elem=document.createElement("script");
		this.src=src;

		this.storage=storage;
	}

	checkBlobSupport(){
		return(Modernizr.blobconstructor && Modernizr.bloburls)?
					Promise.resolve():
					Promise.reject();
	}

	fillSrcBlob(response){
		this.checkBlobSupport()?
			this.elem.src=window.URL.createObjectURL(new Blob([response]))
			:this.elem.innerText=response;

		return Promise.resolve(this);
	}

	fillSrc(){
		this.elem.src=this.src;
		return Promise.resolve(this);
	}

	insertScriptTag(){
		return new Promise((resolve,reject)=>{
			this.elem.onload=()=>resolve();
			this.elem.onerror=()=>reject();

			document.head.appendChild(this.elem);
		})
	}

	checkExistInLocalforage(){
		if (typeof localforage !== "undefined") {
			return localforage.getItem(this.src)
				.then((val)=>val);
		}else{
			return false;
		}
	}

	setInLocalforage(response){
		return localforage.setItem(this.src, response)
			.then(()=>Promise.resolve(response))
			.catch(()=>Promise.reject(response));
	}

	getScript(){
		let request = new XMLHttpRequest();

		request.open("GET", this.src, true);
		//request.overrideMimeType("text/css");

		this.checkBlobSupport().then(()=>request.responseType = 'arraybuffer');


		request.onload = ()=> {
			if (request.status >= 200 && request.status < 400) {
				return Promise.resolve(request.response);
			} else {
				return Promise.reject(Error(request.statusText));
			}
		};

		request.onerror = ()=> {
			return Promise.reject(Error("Сетевая ошибка"));
		};

		request.send();
	}

	preloadScript(){
		switch (this.storage) {
			case "none":
				return this.fillSrc();

				break;
			case "local":
				return this.checkBlobSupport()
							.then(()=>this.checkExistInLocalforage())
							.then((val)=>!val ?
								this.getScript().then((response)=>this.setInLocalforage(response)) :
								this.setInLocalforage(val))

							.then((response)=>this.fillSrcBlob(response))
							.catch((response)=>this.fillSrc(response));
				break;
		}
	}

	loadScript(){
		return this.preloadScript().then(()=>this.insertScriptTag());
	}
}

function loadScripts(scriptArr,storage){
	if(scriptArr.length){
		return scriptArr.map((script)=>(new ScriptLoad(script,storage)).preloadScript())
						.reduce((sequence, scriptPromise) =>
							sequence.then(() =>
									scriptPromise
							).then((script)=>
									script.insertScriptTag()
							), Promise.resolve());
	}else{
		return Promise.reject(Error("Массив скриптов на загрузку пуст"));
	}

}

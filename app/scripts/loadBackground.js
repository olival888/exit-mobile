"use strict";

class BackgroundLoad{
	constructor(elem, folder, storage="none") {
		function makeUrl(fileName, folder) {
			let getFormat = ()=>{
				if(document.documentElement.classList.contains("webp")){
					return "webp";
				}else if(Modernizr.jp2){
					return "jp2";
				}else{
					return "jpg";
				}
			};

			let dpr = window.devicePixelRatio >= 1.5 ? "2x" : "1x";
			let resolutionFolder = (screenWidth)=>{
				if(screenWidth>=900){
					return "1000";
				}else if(screenWidth>=650){
					return "800";
				}else if(screenWidth>=500){
					return "650";
				}else{
					return "500";
				}
			};

			return `${folder}/${getFormat()}/${dpr}/${resolutionFolder(Math.max(window.screen.width,window.screen.height))}/${fileName}.${getFormat()}`;
		}

		this.src=makeUrl(elem.dataset.src, folder);
		this.storage=storage;
		this.elem=elem;
		this.style=document.createElement("style");
	}

	checkFilereaderSupport(){
		return (Modernizr.filereader && Modernizr.datauri)?
			Promise.resolve():
			Promise.reject();
	}

	fillSrcBase64(response) {
		let reader = new window.FileReader();
		reader.readAsDataURL(response);

		reader.onloadend = () => {
			let style=(/landscape/.test(this.src)) ?
				`@media all and (orientation : landscape) {.quest__img{background:url('${reader.result}') no-repeat center/cover;display:block !important}}`:
				`@media all and (orientation : portrait) {.quest__img{background:url('${reader.result}') no-repeat center/cover;display:block !important}}`;
			this.style.innerHTML = style;

			document.head.appendChild(this.style);

			return Promise.resolve();
		};
	}

	fillSrc(){
		let style=(/landscape/.test(this.src)) ?
						`@media all and (orientation : landscape) {.quest__img{background:url('${this.src}') no-repeat center/cover;display:block !important}}`:
						`@media all and (orientation : portrait) {.quest__img{background:url('${this.src}') no-repeat center/cover;display:block !important}}`;
		this.style.innerHTML = style;

		document.head.appendChild(this.style);

		return Promise.resolve();
	}

	checkExistInLocalforage(){
		if (typeof localforage !== "undefined") {
			return localforage.getItem(this.src)
				.then((val)=>val);
		}else{
			return false;
		}
	}

	setInLocalforage(response){
		return localforage.setItem(this.src, response)
			.then(()=>Promise.resolve(response))
			.catch(()=>Promise.reject(response));
	}

	getBase64Img(){
		return new Promise((resolve, reject) => {
			let request = new XMLHttpRequest();

			request.open("GET", this.src, true);
			request.responseType = 'blob';

			request.onload = ()=> {
				if (request.status >= 200 && request.status < 400) {
					resolve(request.response);
				} else {
					reject(Error(request.statusText));
				}
			};

			request.onerror = ()=> {
				reject(Error("Сетевая ошибка"));
			};

			request.send();
		});
	}

	successHendler(){
		this.elem.classList.add("complete");
		this.elem.classList.remove("pending");
		return Promise.resolve();
	}

	errorHendler(){
		this.elem.classList.add("error");
		return Promise.reject();
	}

	preloadBackground() {
		this.elem.classList.add("pending");
		switch (this.storage){
			case "none":
				return this.checkFilereaderSupport()

						.then((val)=>this.getBase64Img())

						.then((response)=>this.fillSrcBase64(response));

				break;
			case "local":
				return this.checkFilereaderSupport()

						.then(()=>this.checkExistInLocalforage())
						.then((val)=>!val?this.getBase64Img().then((response)=>this.setInLocalforage(response)):this.setInLocalforage(val))

						.then((response)=>this.fillSrcBase64(response));
		}
	}

	loadBackground(){
		return this.preloadBackground()
			.then(()=>this.successHendler())
			.catch(()=>this.fillSrc())
			.catch(()=>this.errorHendler());
	}
}


function loadBackgrounds(elemArr, folder,storage) {
	if(elemArr.length){
		return Array.from(elemArr).map((elem)=>(new BackgroundLoad(elem, folder, storage)).preloadBackground())
			.reduce((sequence, bgPromise) =>
				sequence.then(() =>bgPromise)
					.then(()=>this.successHendler())
					.catch(()=>this.fillSrc())
					.catch(()=>this.errorHendler()),
			Promise.resolve());
	}else{
		return Promise.reject(Error("Массив картинок на загрузку пуст"));
	}
}

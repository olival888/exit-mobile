"use strict";

class StyleLoad{
	constructor(elem) {
		function makeUrl(href) {
			/*if (Modernizr.woff2){
				return href.replace(/\.css$/,(match)=>`.woff2${match}`);
			} else if(Modernizr.woff){*/
				return href.replace(/\.css$/,(match)=>`.woff${match}`);
			/*} else if(Modernizr.ttf){
				return href.replace(/\.css$/,(match)=>`.ttf${match}`);
			} else if(Modernizr.eot){
				return href.replace(/\.css$/,(match)=>`.eot${match}`);
			}*/
		}
		this.elem=elem;
		this.href=makeUrl(elem.dataset.href);
	}

	checkBlobSupport(){
		return new Promise((resolve, reject) => {
			(Modernizr.blobconstructor && Modernizr.bloburls && Modernizr.filereader)?
				resolve():
				reject();
		});
	}
	fillHrefBlob(response) {
		return new Promise((resolve, reject) => {
			this.elem.onload=()=>resolve(response);
			this.elem.onerror=()=>reject(response);

			this.elem.href=window.URL.createObjectURL(new Blob([response],{type:"text/css"}));
		});
	}
	fillStyle(response) {
		let styleTag=document.createElement('style');

		styleTag.innerText=response;

		document.head.appendChild(styleTag);
		return Promise.resolve();
	}

	insertStyle(response){
		this.checkBlobSupport()
			.then(()=>this.fillHrefBlob(response))
			.catch(()=>this.fillStyle(response));
	}

	fillHref(){
		return new Promise((resolve, reject) => {
			this.elem.onload=()=>resolve();
			this.elem.onerror=()=>reject();

			this.elem.href=this.href;
		});
	}
	getStyle(){
		return new Promise((resolve, reject) => {
			localforage.getItem(this.href)
				.then((val)=>{
					if(!val){
						let request = new XMLHttpRequest();

						request.open("GET", this.href, true);

						this.checkBlobSupport().then(()=>request.responseType = 'arraybuffer');


						request.onload = ()=> {
							if (request.status >= 200 && request.status < 400) {
								localforage.setItem(this.href, request.response)
									.then(()=>resolve(request.response))
									.catch(reject);

							} else {
								reject(Error(request.statusText));
							}
						};

						request.onerror = ()=> {
							reject(Error("Сетевая ошибка"));
						};

						request.send();
					}else{
						resolve(val);
					}
				});
		});
	}
	loadStyle(){
		return this.getStyle()
				.then((response)=>this.insertStyle(response))

				.catch(()=>this.fillHref());
	}
}

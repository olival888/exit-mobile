function mapInit() {
	var mapCenter=new google.maps.LatLng(55.751900, 37.587222);

	var mapOptions = {
		center: mapCenter,
		zoom:16,
		minZoom: 12,
		maxZoom:16,
    zoomControl: false,
     /*zoomControlOptions: {
			style: google.maps.ZoomControlStyle.SMALL
		},*/
		disableDoubleClickZoom: true,
		mapTypeControl: false,
		scaleControl: false,
		scrollwheel: true,
		panControl: false,
		streetViewControl: false,
		draggable: true,
		overviewMapControl: false,
		overviewMapControlOptions: {
			opened: false
		},
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles: [
			{ stylers: [
				{ saturation: -100 },
				{ gamma: 1 }
			] },
			{ elementType: "labels.text.stroke", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.business", elementType: "labels.text", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.business", elementType: "labels.icon", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.place_of_worship", elementType: "labels.text", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "poi.place_of_worship", elementType: "labels.icon", stylers: [
				{ visibility: "off" }
			] },
			{ featureType: "road", elementType: "geometry", stylers: [
				{ visibility: "simplified" }
			] },
			{ featureType: "water", stylers: [
				{ visibility: "on" },
				{ saturation: 50 },
				{ gamma: 0 },
				{ hue: "#50a5d1" }
			] },
			{ featureType: "administrative.neighborhood", elementType: "labels.text.fill", stylers: [
				{ color: "#333333" }
			] },
			{ featureType: "road.local", elementType: "labels.text", stylers: [
				{ weight: 0.5 },
				{ color: "#333333" }
			] },
			{ featureType: "transit.station", elementType: "labels.icon", stylers: [
				{ gamma: 1 },
				{ saturation: 50 }
			] }
		]
	};

	var map = new google.maps.Map(document.getElementById('map'), mapOptions);

	var marker = new google.maps.Marker({
		position: map.getCenter(),
		map: map,
		title: 'EXIT',
		icon:'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4xLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL0dyYXBoaWNzL1NWRy8xLjEvRFREL3N2ZzExLmR0ZCI+DQo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjUxLjlweCIgaGVpZ2h0PSI5MXB4IiB2aWV3Qm94PSIwIDAgNTEuOSA5MSI+DQoJPHBhdGggZD0iTTI2LDBDMTEuNiwwLDAsMTEuNiwwLDI2YzAsNS44LDEuOSwxMS4yLDUuMiwxNS42YzAsMCwwLDAsMCwwQzUuOCw0Mi40LDI1LjcsNzEuNiwyNiw5MWMwLjMtMTkuNCwyMC4xLTQ4LjUsMjAuOC00OS40YzAsMCwwLDAsMCwwYzMuMi00LjMsNS4yLTkuNyw1LjItMTUuNkM1MS45LDExLjYsNDAuMywwLDI2LDAiLz4NCjwvc3ZnPg0K'
	});

	document.addEventListener("click",function(e){
		if(e.target && e.target.nodeName == "LABEL" && e.target.className == "views-list__label"){
			map.panTo(mapCenter);
			map.setZoom(16);
		}
	});

	google.maps.event.addListener(marker, 'click', function(event) {
		window.open('https://goo.gl/maps/8buki')
	});




	/*var myPanoid = "QKZJDF5QWO0AAAAAAAABOw";
	var panoramaOptions = {
		pano: myPanoid,
		pov: {
			heading: 45,
			pitch:-2
		},
		zoom: 1
	};
	var myPhotoSphere = new google.maps.StreetViewPanorama(
		document.getElementById('sphere'),
		panoramaOptions);
	myPano.setVisible(true);*/
}

google.maps.event.addDomListener(window, 'load', mapInit);

if(!Modernizr.calcpercent){
	var mainHeight=window.innerHeight-100;
	document.querySelector('.map').style.height=(mainHeight-170)+'px';
}

//<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m10!1m3!1d2245.334092121223!2d37.5942657!3d55.75269720000001!2m1!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54a4c8531609d%3A0x12029e3e2487553a!2z0YPQuy4g0J3QvtCy0YvQuSDQkNGA0LHQsNGCLCDQnNC-0YHQutCy0LA!5e0!3m2!1sru!2sru!4v1410869028103" width="800" height="600" frameborder="0" style="border:0"></iframe>

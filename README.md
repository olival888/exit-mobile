# Exit-mobile
Мобильная версия сайта **[Exit.am](http://exit.am)**

#### Установка
Необходимо глобально установить Gulp и Bower:

```sh
$ npm install -g gulp bower
```

```sh
$ git clone git@gitlab.com:olival888/exit-mobile.git
$ cd exit-mobile
$ npm install && bower install
```

Для просмотра проекта:

```sh
$ gulp watch
```

Для сборки пректа:

```sh
$ gulp
```

Так же обязательно должен быть установлен **[ImageMagic](https://www.npmjs.com/package/gulp-image-resize)**

Исходники проекта содержаться в папке **exit-mobile/app**

Собраный проект содержится в папке **exit-mobile/dist**

#### Страницы
- [Главная](http://localhost:9000)
- [Расписание](http://localhost:9000/timetable.html)
- [Бронирование](http://localhost:9000/booking.html)
- [Личный кабинет](http://localhost:9000/user-page.html)
- [Контакты](http://localhost:9000/contacts.html)
- [Правила](http://localhost:9000/rules.html)
- [Контакты](http://localhost:9000/contacts.html)
- [Регистрация](http://localhost:9000/login.html)
- [Ввод телефона после регистрации через соц сети](http://localhost:9000/tel-auth.html)

#### Версия
0.9.2

#### ToDo
- Попробывать вынести тест на .jp2, сделать сами картинки и внедрить их на сайт
- Сделать эффект на начавшемся квесте стилями css или js(для осла)
- Поставить нормальные прелодеры
- Реализовать нативную валидацию

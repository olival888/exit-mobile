/*
 * ToDo
 *
 * 3) Заливка по ssh
 * */



'use strict';

var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var browserSyncConfig={
	reloadOnRestart: true,
	notify: false,
	port: 9000,
	startPath: "/",
	server: {
		baseDir: ['.tmp', 'app'],
		routes: {
			'/bower_components': 'bower_components'
		}
	}
};

var jadeData = require('./data.json');

//вставить условие, чтобы писались только нужные файлы, без _
gulp.task('views', function () {
	return gulp.src(['app/tamplates/**/*.jade'])
		.pipe($.plumber())

		//only pass unchanged *main* files and *all* the partials
		.pipe($.changed('.tmp', {extension: '.html'}))

		//filter out unchanged partials, but it only works when watching
		.pipe($.if(browserSync.active, $.cached('jade')))

		//find files that depend on the files that have changed
		.pipe($.jadeInheritance({basedir: 'app/tamplates'}))

		//filter out partials (folders and files starting with "_" )
		.pipe($.filter(function (file) {
			return !/\_/.test(file.path) && !/^_/.test(file.relative);
		}))

		.pipe($.jade({
			locals: jadeData,
			pretty: true
		}))
		.pipe($.beml())
		.pipe($.fileInclude({basepath: '.tmp'}))
		.pipe(gulp.dest('.tmp'))
		.pipe(reload({stream: true}));
});

gulp.task('styles', function () {
	//return gulp.src('app/styles/**/*.scss')
	/*//only pass unchanged *main* files and *all* the partials
	 //.pipe($.changed('.tmp/styles', {extension: '.css'}))
	 //filter out unchanged scss files, only works when watching
	 .pipe($.if(browserSync.active, $.cached('scss')))

	 //find files that depend on the files that have changed
	 .pipe($.sassInheritance({dir: 'app/styles/'}))

	 //filter out internal imports (folders and files starting with "_" )
	 .pipe($.filter(function (file) {
	 return !/\_/.test(file.path) || !/^_/.test(file.relative);
	 }))*/

	//.pipe($.sourcemaps.init())
	$.rubySass('app/styles', {
		style: 'expanded',
		precision: 10,
		sourcemap: true
	})
		.on('error', function (err) {
			console.error('Error!', err.message);
		})
		.pipe($.postcss([
			require('autoprefixer-core')({browsers: ['last 1 version']})
		]))
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('.tmp/styles'))
		.pipe(reload({stream: true}));
});

gulp.task('scripts', function () {
	return gulp.src(['app/scripts/**/*.js'])
		.pipe($.plumber())
		.pipe($.sourcemaps.init())
		.pipe($.babel())
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('sprites', function () {
	return gulp.src('app/images/svg/sprites/*svg')
		.pipe($.imagemin({
			plugins: [
				{cleanupIDs: false}
			]
		}))
		.pipe($.svgstore({ inlineSvg: true }))
		.pipe(gulp.dest('.tmp/images/svg'));
});

gulp.task('icons', function () {
	return gulp.src('app/images/svg/icons/**/*.svg')
		.pipe($.imagemin({
			plugins: [
				{cleanupIDs: false}
			]
		}))
		.pipe(gulp.dest('.tmp/images/svg/icons'));
});

gulp.task('svg', $.sync(gulp).async(['sprites','icons']));

/*gulp.task('modernizr', function () {
 gulp.src(['app/scripts/!*.js', '.tmp/styles/!*.scss'])
 .pipe($.modernizr({
 "options": [
 //"setClasses",
 "addTest",
 //"addAsyncTest",
 //"html5printshiv",
 //"testProp",
 //"fnBind",
 "testStyles"
 ],
 //customTests: ["app/scripts/modernizr/woff2.test.js"]
 }))
 .pipe(gulp.dest("app/scripts/modernizr"))
 });*/

var fs = require('fs');
var path = require('path');
var es = require('event-stream');

function getFolders(rootDir){/*улучшить*/
	var dirList=[];

	function getChildFolders(dir){
		fs.readdirSync(dir).filter(function(file){
			if(fs.statSync(path.join(dir, file)).isDirectory()){
				getChildFolders(dir+'/'+file);
			}else{
				var url=dir.replace('app/','');
				if(dirList.indexOf(url)==-1){
					dirList.push(url);
				}
			}
		});
	}

	getChildFolders(rootDir);

	return dirList;
}

var dir='app/images/media/';
var folders= getFolders(dir);
var postfix="/2x/";

var resizeImgTasks=[
	1000,
	800,
	650,
	500
];

resizeImgTasks.forEach(function(width) {
	gulp.task('resize-img-'+width, function () {
		var tasks=folders.map(function(folder) {
			return gulp.src(path.join(folder,'/*.{tif,tiff,jpg}'), { cwd: 'app/' })
				.pipe($.gm(function (gmfile) {
					if(/.*landscape.*/.test(gmfile.source) || /.*common.*/.test(gmfile.source) || /.*main.*/.test(gmfile.source)){
						return gmfile.resize(width*2, null);
					}else if(/.*booking.*portrait.*/.test(gmfile.source) || /.*timetable.*portrait.*/.test(gmfile.source)){
						return gmfile.resize(null, width*2);
					}
				}, {
					imageMagick: true
				}))

				.pipe($.size({title:"Folder: "+path.join(folder,"/tiff",postfix,"/"+width),showFiles:true,gzip: false}))

				.pipe(gulp.dest(path.join(folder,"/tiff",postfix,"/"+width), { cwd: '.tmp/' }));
		});

		return es.concat.apply(null, tasks);
	});
});

var convertDir='.tmp/images/media';

gulp.task('convert-webp', function () {
	var folders= getFolders(convertDir);

	var tasks=folders.map(function(folder) {
		var destFolder=/.*tiff.*/.test(folder)?folder.replace('tiff','webp'):folder.replace('tif','webp');

		return gulp.src(path.join(folder,'/*.{tif,tiff,jpg}'))
			.pipe($.plumber())
			.pipe($.gm(function (gmfile) {
				return gmfile.define('webp:lossless=false')
					.define('webp:method=6')
					.quality(/\.jpg$/.test(gmfile.source)?85:35)
					.setFormat('webp');
			},{
				imageMagick:true
			}))
			.pipe($.size({title:"Folder: "+destFolder,showFiles:true,gzip: false}))
			.pipe(gulp.dest(destFolder));
	});

	return es.concat.apply(null, tasks);
});

/*gulp.task('convert-jp2', function () {
 var folders= getFolders(convertDir);

 var tasks=folders.map(function(folder) {
 var destFolder=/.*tiff.*.?/.test(folder)?folder.replace('tiff','jp2'):folder.replace('tif','jp2');

 return gulp.src(path.join(folder,'/*.{tif,tiff,jpg}'))
 .pipe($.plumber())
 .pipe($.gm(function (gmfile) {
 return gmfile.define('jp2:rate=85')
 .define('jp2:tilewidth=1024')
 .define('jp2:tileheight=1024')
 //.quality(45)
 .setFormat('jp2');
 },{
 imageMagick:true
 }))
 .pipe($.size({title:"Folder: "+destFolder,showFiles:true,gzip: false}))
 .pipe(gulp.dest(destFolder));
 });

 return es.concat.apply(null, tasks);
 });*/

gulp.task('convert-jpeg', function () {
	var folders= getFolders(convertDir);

	var tasks=folders.map(function(folder) {
		var destFolder=/.*tiff.*/.test(folder)?folder.replace('tiff','jpg'):folder.replace('tif','jpg');
		return gulp.src(path.join(folder,'/*.{tif,tiff,jpg}'))
			.pipe($.plumber())
			.pipe($.gm(function (gmfile) {
				return gmfile.quality((/\.jpg$/.test(gmfile.source))?85:30)
					.setFormat('jpg');
			},{
				imageMagick:true
			}))
			.pipe($.imagemin({progressive: true,interlaced: true}))
			.pipe($.size({title:"Folder: "+destFolder,showFiles:true,gzip: false}))
			.pipe(gulp.dest(destFolder));
	});

	return es.concat.apply(null, tasks);
});

gulp.task('convert-jxr', function () {
	var folders= getFolders(convertDir);

	var tasks=folders.map(function(folder) {
		var destFolder=/.*tiff.*/.test(folder)?folder.replace('tiff','jxr'):folder.replace('tif','jxr');

		return gulp.src(path.join(folder,'/*.{tif,tiff,jpg}'))
			.pipe($.plumber())
			.pipe($.gm(function (gmfile) {
				return gmfile//.quality(30)
					.setFormat('jxr');
			},{
				imageMagick:true
			}))
			.pipe($.size({title:"Folder: "+destFolder,showFiles:true,gzip: false}))
			.pipe(gulp.dest(destFolder));
	});

	return es.concat.apply(null, tasks);
});

gulp.task("convert",$.sync(gulp).sync(['convert-webp',/*'convert-jp2',*/'convert-jpeg'/*,'convert-jxr'*/]));

gulp.task('cleanTIFF', require('del').bind(null, ['.tmp/**/tiff']));

gulp.task('img-ready', $.sync(gulp).sync((!fs.existsSync(".tmp/") || !fs.existsSync(".tmp/images/") || !fs.existsSync(".tmp/images/media"))?
	['resize-img-1000','resize-img-800','resize-img-650','resize-img-500',"convert", "cleanTIFF"]:
	$.util.noop()));

gulp.task('favicons', function () {
	/*return gulp.src('.tmp/index.html')
	 //.pipe($.plumber())
	 .pipe($.favicons({
	 files: { dest: 'images/favicons/' },
	 settings: { background: '#ffffff' }
	 }))
	 .pipe(gulp.dest('dest'));
	 */
	/*return gulp.src('app/favicon.png')
	 .pipe($.favicons({
	 files:{html:"index.html"},
	 settings: { background: '#1d1d1d' , vinylMode: true }
	 }, function(code) {
	 console.log(code);
	 }))
	 .pipe(through.obj(function (file, enc, cb) {
	 console.log(file.path);
	 this.push(file);
	 cb();
	 }))
	 .pipe(gulp.dest('.tmp/images/favicons'));*/
});

gulp.task('jshint', function () {
	return gulp.src('app/scripts/**/*.js')
		.pipe($.plumber())
		.pipe($.jshint())
		.pipe($.jshint.reporter('jshint-stylish'))
		.pipe($.jshint.reporter('fail'));
});

/*require('postcss-svg/lib/reload.js')(require);
 var postcssInlineSVG = require('postcss-svg');*/

gulp.task('useref', function () {
	var assets = $.useref.assets({searchPath: ['.tmp',"."]});

	//postcssInlineSVG = require.reload('postcss-svg');


	return gulp.src('.tmp/*.html')
		.pipe(assets)
		.pipe($.if('*.css', $.postcss([
			require('csswring')()
		])))
		//.pipe($.if('*.js', $.uglify()))
		.pipe(assets.restore())
		.pipe($.if('*.css', $.postcss([
			//попробывать переделать относительные пути в абсолютные
			require('postcss-url')({url: "inline"})
			/*,
			 postcssInlineSVG({
			 paths: ['app'],
			 debug: true,
			 svgo: true,
			 ei: false
			 })*/
		])))
		.pipe($.useref())
		//.pipe($.if('*.html', $.minifyHtml({conditionals: true, loose: true})))
		.pipe(gulp.dest('dist'));
});



gulp.task('img-min', function () {
	return gulp.src(['app/images/**/*.{jpg,png}',"!app/images/media/**/*","!app/images/favicons/**/*"])
		.pipe($.imagemin({progressive: true,interlaced: true}))
		.pipe(gulp.dest('.tmp/images'));
});

gulp.task('images', function () {
	return gulp.src(".tmp/images/**/*.*")
		.pipe(gulp.dest("dist/images"));
});

gulp.task('extras', function () {
	return gulp.src([
		'app/*.*',
		'!app/*.jade'
	], {
		dot: true
	}).pipe(gulp.dest('dist'));
});

gulp.task('clean', require('del').bind(null, ['.tmp', 'dist']));


// inject bower components
gulp.task('wiredep', function () {
	var wiredep = require('wiredep').stream;

	gulp.src('app/styles/*.scss')
		.pipe(wiredep())
		.pipe(gulp.dest('app/styles'));

	gulp.src('app/*.jade')
		.pipe(wiredep({ignorePath: /^(\.\.\/)*\.\./}))
		.pipe(gulp.dest('app'));
});

gulp.task('serve', $.sync(gulp).sync(['img-ready','svg','img-min', ['views', 'styles', 'scripts']]), function () {
	browserSync.init(browserSyncConfig);

	// watch for changes
	gulp.watch([
		'.tmp/scripts/**/*.js',
		'app/images/**/*'
	]).on('change', reload);

	gulp.watch('app/scripts/**/*.js', ['scripts']);
	gulp.watch('app/styles/**/*.scss', ['styles']);
	gulp.watch('app/**/*.jade', ['views']);
	gulp.watch('app/images/svg/sprites/**/*', ['sprites']);
	gulp.watch('app/images/svg/icons/**/*', ['icons']);
	gulp.watch(['app/images/**/*.{jpg,png}',"!app/images/media/**/*","!app/images/favicons/**/*"], ['img-min']);
	gulp.watch('bower.json', ['wiredep']);
});

gulp.task('scripts:test', function () {
	return gulp.src('test/spec_es6/*.js')
		.pipe($.plumber())
		.pipe($.sourcemaps.init())
		.pipe($.babel())
		.pipe($.sourcemaps.write('.'))
		.pipe(gulp.dest('test/spec'));
});

gulp.task('serve:test', ['scripts','scripts:test'], function () {
	browserSync({
		notify: false,
		port: 9001,
		ui: false,
		server: {
			baseDir: ['test','.tmp'],
			routes: {
				'/bower_components': 'bower_components'
			}
		}
	});

	gulp.watch(['.tmp/scripts/**/*.js','test/spec/**/*.js','test/*.html']).on('change', reload);
	gulp.watch('test/spec_es6/**/*.js', ['scripts:test']);
});

gulp.task('watch', function () {
	gulp.start('serve');
});

gulp.task('build', $.sync(gulp).sync(['img-ready','svg','img-min', ['views', 'styles', 'scripts', ['images','useref', 'extras']]]), function () {
	return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: false}));
});

gulp.task('default', ['clean'], function () {
	gulp.start('build');
});
